#!/usr/bin/env bash

CMDS="java mvn cqlsh spark-submit"

for i in $CMDS
do
	command -v $i >/dev/null && continue || { echo "Command not found: '$i'. Install correspondent package or export home variable."; exit 1; }
done

response=$(curl 'http://127.0.0.1:8080/currency' --write-out %{http_code} --silent --output /dev/null)
echo $response
if [ $response -ne 200 ]; then
  echo "Currency service not found.";
  exit 1;
fi

echo "Building infare-exercise project..."
mvn -T 4 -U clean package --log-file build-log.txt

echo "Preparing Cassandra..."
cqlsh -f ./scripts/init.cql localhost

echo "Starting rest service..."
java -jar ./data-access-service/target/data-access-service-1.0-SNAPSHOT.jar &

echo "Executing data-loader job..."
bash ./spark-jobs/data-loader-job/bin/loader-job.sh

echo "Executing currency-converter job..."
bash ./spark-jobs/currency-converter-job/bin/converter-job.sh
