package access.service;

import access.model.FlightDataByWeek;
import access.repository.FlightDataByWeekRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.query.CassandraPageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightDataService {

    @Autowired
    private FlightDataByWeekRepository repository;

    public List<FlightDataByWeek> pagedFlightDataByWeek(short infareweek, int page, int size) {
        return repository.findByObservedWeekAsInfareweek(infareweek, CassandraPageRequest.of(page, size)).getContent();
    }
}
