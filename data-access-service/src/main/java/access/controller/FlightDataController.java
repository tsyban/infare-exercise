package access.controller;

import access.model.FlightDataByWeek;
import access.service.FlightDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FlightDataController {

    @Autowired
    private FlightDataService flightDataService;

    @GetMapping("/flightdata/{infareweek}")
    public List<FlightDataByWeek> getEmployees(@PathVariable short infareweek,
                                               @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                               @RequestParam(value = "size", required = false, defaultValue = "100") int size) {
        return flightDataService.pagedFlightDataByWeek(infareweek, page, size);
    }

}
