package access.model;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table("flight_data_by_week")
public class FlightDataByWeek {

    @PrimaryKeyColumn(name = "observed_week_as_infareweek", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private Short infareWeek;
    @PrimaryKeyColumn(name = "carrier_id", ordinal = 1, type = PrimaryKeyType.CLUSTERED)
    private String carrierId;
    @PrimaryKeyColumn(name = "id", ordinal = 2,type = PrimaryKeyType.CLUSTERED)
    private UUID id;
    @Column("trip_price_min")
    private Double tripPriceMin;
    @Column("trip_price_max")
    private Double tripPriceMax;
    @Column("trip_price_avg")
    private Double tripPriceAvg;
    @Column("observed_date_min_as_infaredate")
    private Short observedDateMinAsInfaredate;
    @Column("observed_date_max_as_infaredate")
    private Short observedDateMaxAsInfaredate;
    @Column("full_weeks_before_departure")
    private Integer fullWeeksBeforeDeparture;
    @Column("searched_cabin_class")
    private String searchedCabinClass;
    @Column("booking_site_id")
    private Integer bookingSiteId;
    @Column("booking_site_type_id")
    private Short bookingSiteTypeId;
    @Column("is_trip_one_way")
    private Short isTripOneWay;
    @Column("trip_origin_airport_id")
    private Integer tripOriginAirportId;
    @Column("trip_destination_airport_id")
    private Integer tripDestinationAirportId;
    @Column("trip_min_stay")
    private Short tripMinStay;

    public Short getInfareWeek() {
        return infareWeek;
    }

    public void setInfareWeek(Short infareWeek) {
        this.infareWeek = infareWeek;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Double getTripPriceMin() {
        return tripPriceMin;
    }

    public void setTripPriceMin(Double tripPriceMin) {
        this.tripPriceMin = tripPriceMin;
    }

    public Double getTripPriceMax() {
        return tripPriceMax;
    }

    public void setTripPriceMax(Double tripPriceMax) {
        this.tripPriceMax = tripPriceMax;
    }

    public Double getTripPriceAvg() {
        return tripPriceAvg;
    }

    public void setTripPriceAvg(Double tripPriceAvg) {
        this.tripPriceAvg = tripPriceAvg;
    }

    public Short getObservedDateMinAsInfaredate() {
        return observedDateMinAsInfaredate;
    }

    public void setObservedDateMinAsInfaredate(Short observedDateMinAsInfaredate) {
        this.observedDateMinAsInfaredate = observedDateMinAsInfaredate;
    }

    public Short getObservedDateMaxAsInfaredate() {
        return observedDateMaxAsInfaredate;
    }

    public void setObservedDateMaxAsInfaredate(Short observedDateMaxAsInfaredate) {
        this.observedDateMaxAsInfaredate = observedDateMaxAsInfaredate;
    }

    public Integer getFullWeeksBeforeDeparture() {
        return fullWeeksBeforeDeparture;
    }

    public void setFullWeeksBeforeDeparture(Integer fullWeeksBeforeDeparture) {
        this.fullWeeksBeforeDeparture = fullWeeksBeforeDeparture;
    }

    public String getSearchedCabinClass() {
        return searchedCabinClass;
    }

    public void setSearchedCabinClass(String searchedCabinClass) {
        this.searchedCabinClass = searchedCabinClass;
    }

    public Integer getBookingSiteId() {
        return bookingSiteId;
    }

    public void setBookingSiteId(Integer bookingSiteId) {
        this.bookingSiteId = bookingSiteId;
    }

    public Short getBookingSiteTypeId() {
        return bookingSiteTypeId;
    }

    public void setBookingSiteTypeId(Short bookingSiteTypeId) {
        this.bookingSiteTypeId = bookingSiteTypeId;
    }

    public Short getIsTripOneWay() {
        return isTripOneWay;
    }

    public void setIsTripOneWay(Short isTripOneWay) {
        this.isTripOneWay = isTripOneWay;
    }

    public Integer getTripOriginAirportId() {
        return tripOriginAirportId;
    }

    public void setTripOriginAirportId(Integer tripOriginAirportId) {
        this.tripOriginAirportId = tripOriginAirportId;
    }

    public Integer getTripDestinationAirportId() {
        return tripDestinationAirportId;
    }

    public void setTripDestinationAirportId(Integer tripDestinationAirportId) {
        this.tripDestinationAirportId = tripDestinationAirportId;
    }

    public Short getTripMinStay() {
        return tripMinStay;
    }

    public void setTripMinStay(Short tripMinStay) {
        this.tripMinStay = tripMinStay;
    }
}
