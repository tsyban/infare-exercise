package access.repository;

import access.model.FlightDataByWeek;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FlightDataByWeekRepository extends CrudRepository<FlightDataByWeek, UUID> {

    @Query("select * from infare_exercise.flight_data_by_week where observed_week_as_infareweek = :infareWeek")
    Slice<FlightDataByWeek> findByObservedWeekAsInfareweek(@Param("infareWeek") short infareweek, Pageable pageable);

}
