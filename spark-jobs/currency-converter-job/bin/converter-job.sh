#!/usr/bin/env bash

start_week=600
weeks_to_process=10

spark-submit \
    --conf spark.sql.shuffle.partitions=50 \
    --class converter.Application \
    --master local \
    --driver-memory 1g \
    --executor-memory 1g \
    spark-jobs/currency-converter-job/target/currency-converter-job-1.0-SNAPSHOT-jar-with-dependencies.jar "${start_week}" "${weeks_to_process}"