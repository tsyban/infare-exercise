package converter

import java.util.concurrent.Executors

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}
import scala.util.{Failure, Success, Try}

object Application extends App {

  val logger = Logger.getLogger(Application.getClass)

  logger.info("Starting price converter job")

  val inputArgs = InputArgs(args)
  val jobConfig = JobConfig()
  logger.info(inputArgs)
  logger.info(jobConfig)

  implicit val spark: SparkSession = Spark.sparkSession(jobConfig)
  implicit val ec: ExecutionContextExecutor = ExecutionContext.fromExecutor(Executors.newWorkStealingPool(jobConfig.currencyServiceParallelism))

  val priceService = PriceService(jobConfig.currencyServiceURL)
  val weeklyConverter = WeeklyConverter(priceService)

  def onSuccess(week: Int, rowsProcessed: Long): Unit = logger.info(s"Week $week successfully converted. Rows processed: $rowsProcessed")

  def onFailure(week: Int, e: Throwable): Unit = logger.error(s"Failed to convert week $week.", e)

  val endWeek = inputArgs.startWeek + inputArgs.weeksToProcess
  for(infareweek <- inputArgs.startWeek until endWeek){
    Try(weeklyConverter.convertWeek(infareweek)) match {
      case Success(v) => onSuccess(infareweek, v)
      case Failure(e) => onFailure(infareweek, e)
    }
  }

  logger.info("Shutdown price converter job")
}
