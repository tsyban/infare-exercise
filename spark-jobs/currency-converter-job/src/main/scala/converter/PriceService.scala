package converter

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import converter.PriceService.RequestPriceException
import scalaj.http.Http

import scala.collection.Map
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

object PriceService {

  case class Converted(price: BigDecimal)

  case class RequestPriceException(throwable: Throwable) extends RuntimeException(throwable)

  def apply(url: String)(implicit ec: ExecutionContext): PriceService = new PriceService(url)
}

class PriceService(url: String)(implicit ec: ExecutionContext) {

  private val cache = lruCache(500)

  def convertPrices(prices: List[BigDecimal]): Map[BigDecimal, BigDecimal] = {
    val pricesCached = prices.filter(p => cache.containsKey(p.bigDecimal)).map(p => p -> cache.get(p.bigDecimal)).toMap
    val pricesToRequest = prices.filterNot(cache.containsKey(_))
    val priceToFuture = pricesToRequest.distinct.map(price => price -> requestConvertedAsync(price)).toMap
    val futures = Future.sequence(priceToFuture.values)
    Await.result(futures, Duration.Inf)
    val convertedPrices = priceToFuture.mapValues(mapResult)
    convertedPrices.foreach(t => cache.put(t._1, t._2))
    convertedPrices ++ pricesCached
  }

  private def mapResult(future: Future[BigDecimal]) = future.value match {
    case Some(Success(price)) => price
    case Some(Failure(exception)) => throw RequestPriceException(exception) // TODO failed whole week ?
  }

  private def requestConvertedAsync(price: BigDecimal) = Future { requestConverted(price) }

  private def requestConverted(price: BigDecimal) = {
    val body = Http(url).param("price", price.toString).asString.body
    val mapper = new ObjectMapper
    mapper.registerModule(DefaultScalaModule)
    val map = mapper.readValue(body, classOf[Map[String, Double]])
    BigDecimal(map("price"))
  }

  private def lruCache(capacity: Int): java.util.Map[BigDecimal, BigDecimal] = {
    new util.LinkedHashMap[BigDecimal, BigDecimal](capacity, 0.7F, true) {

      private val cacheCapacity = capacity

      override def removeEldestEntry(entry: java.util.Map.Entry[BigDecimal, BigDecimal]): Boolean = {
        this.size() > this.cacheCapacity
      }
    }
  }


}
