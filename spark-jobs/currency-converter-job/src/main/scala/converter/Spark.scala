package converter

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.cassandra._
object Spark {

  def sparkSession(jobConfig: JobConfig): SparkSession = {
    SparkSession.builder()
      .appName(jobConfig.jobName)
      .getOrCreate()
      .setCassandraConf(Map(
        "spark.cassandra.connection.host" -> jobConfig.cassandraHost,
        "spark.cassandra.connection.port" -> jobConfig.cassandraPort.toString))
  }
}
