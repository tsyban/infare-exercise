package converter

import com.typesafe.config.{Config, ConfigFactory}

case class JobConfig(jobName: String, cassandraHost: String, cassandraPort: Int, currencyServiceURL: String, currencyServiceParallelism: Int)

object JobConfig {

  def defaultCurrencyServiceParallelism = 10
  def currencyServiceParallelismRange = 5 to 100

  def apply(config: Config): JobConfig = {
    val jobName = config.getString("job.name")
    val cassandraHost = config.getString("cassandra.host")
    val cassandraPort = config.getInt("cassandra.port")
    val currencyServiceURL = config.getString("currencyService.url")
    val currencyServiceParallelism = Some(config.getInt("currencyService.parallelism"))
      .filter(currencyServiceParallelismRange contains _)
      .getOrElse(defaultCurrencyServiceParallelism)
    new JobConfig(jobName, cassandraHost, cassandraPort, currencyServiceURL, currencyServiceParallelism)
  }

  def apply(): JobConfig = JobConfig(ConfigFactory.load())
}

