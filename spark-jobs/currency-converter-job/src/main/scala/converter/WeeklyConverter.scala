package converter

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession

object WeeklyConverter {

  def apply(priceService: PriceService)(implicit spark: SparkSession): WeeklyConverter = new WeeklyConverter(priceService)
}

class WeeklyConverter(priceService: PriceService)(implicit spark: SparkSession) {

  val logger: Logger = Logger.getLogger(WeeklyConverter.getClass)

  import spark.implicits._

  def convertWeek(infareweek: Int): Long = {

    logger.info(s"Processing week: $infareweek")

    val inputDF = spark
      .read
      .format("org.apache.spark.sql.cassandra")
      .option("keyspace", "infare_exercise")
      .option("table", "flight_data_by_week")
      .load
      .filter($"observed_week_as_infareweek" === infareweek)
      .drop("trip_price_avg_2")
      .cache()

    val idToPriceMap = inputDF.select("id", "trip_price_avg").rdd
      .map(x => (x.getString(0), x.getDecimal(1)))
      .collectAsMap()
      .mapValues(BigDecimal(_))

    logger.info(s"Prices count to be requested: ${idToPriceMap.size}")

    val convertedPricesMap = priceService.convertPrices(idToPriceMap.values.toList)
    val idToConvertedPriceMap = idToPriceMap.mapValues(convertedPricesMap)

    val convertedPricesDF = idToConvertedPriceMap.toSeq.toDF("id", "trip_price_avg_2")
    val outDF = inputDF.join(convertedPricesDF, Seq("id"))

    outDF
      .write
      .format("org.apache.spark.sql.cassandra")
      .mode("append")
      .option("cassandra.output.batch.size.bytes", Math.pow(10, 6))
      .option("cassandra.output.concurrent.writes", 15)
      .option("keyspace", "infare_exercise")
      .option("table", "flight_data_by_week")
      .save

    idToConvertedPriceMap.size
  }

}
