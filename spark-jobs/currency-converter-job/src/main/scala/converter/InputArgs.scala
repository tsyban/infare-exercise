package converter

case class InputArgs(startWeek: Int, weeksToProcess: Int)

object InputArgs {

  val defaultStartWeek = 700
  val defaultWeeksToProcess = 1

  def apply(args: Array[String]): InputArgs = {
    val lifted = args.lift
    val startWeek = lifted(0).map(_.toInt).getOrElse(defaultStartWeek)
    val weeksToProcess = lifted(1).map(_.toInt).getOrElse(defaultWeeksToProcess)
    new InputArgs(startWeek, weeksToProcess)
  }
}
