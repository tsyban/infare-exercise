package loader

import java.util.UUID

import model.FlightData
import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.{Column, Encoders}

object Application extends App {

  val logger = Logger.getLogger(Application.getClass)

  logger.info("Starting flight data loader job")

  val inputArgs = InputArgs(args)
  val jobConfig = JobConfig()

  val spark = Spark.sparkSession(jobConfig)


  val uuid = udf(() => UUID.randomUUID().toString)
  val infaredateToWeek = (infaredate: Column) => ceil(infaredate / lit(7)).cast(IntegerType) // use ceil for 1 based weeks

  import spark.implicits._

  val inputDF = spark
    .read
    .format("csv")
    .option("delimiter", "\t")
    .schema(Encoders.product[FlightData].schema)
    .load(inputArgs.filesToLoad:_ *)

  val outDF = inputDF
    .withColumn("observed_week_as_infareweek", infaredateToWeek($"observed_date_min_as_infaredate"))
    .withColumn("id", uuid())

  outDF
    .write
    .format("org.apache.spark.sql.cassandra")
    .mode("overwrite")
    .option("confirm.truncate", "true")
    .option("cassandra.output.batch.size.bytes", Math.pow(10, 6))
    .option("cassandra.output.concurrent.writes", 15)
    .option("keyspace", "infare_exercise")
    .option("table", "flight_data_by_week")
    .save()

  logger.info("Shutdown flight data loader job")

}
