package loader

import com.typesafe.config.{Config, ConfigFactory}

case class JobConfig(jobName: String, cassandraHost: String, cassandraPort: Int)

object JobConfig {

  def apply(config: Config): JobConfig = {
    val jobName = config.getString("job.name")
    val cassandraHost = config.getString("cassandra.host")
    val cassandraPort = config.getInt("cassandra.port")
    new JobConfig(jobName, cassandraHost, cassandraPort)
  }

  def apply(): JobConfig = JobConfig(ConfigFactory.load())
}

